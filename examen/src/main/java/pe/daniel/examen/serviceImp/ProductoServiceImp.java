package pe.daniel.examen.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import pe.daniel.examen.dao.ProductoDao;
import pe.daniel.examen.enity.Producto;
import pe.daniel.examen.services.ProductoService;

@Service
public class ProductoServiceImp implements ProductoService {
	@Autowired
	private ProductoDao productoDao;
	
	@Override
	public List<Producto> findAll() {
		// TODO Auto-generated method stub
		return (List<Producto>) productoDao.findAll();
	}

	@Override
	public Producto findById(Long id) {
		// TODO Auto-generated method stub
		return productoDao.findById(id).orElse(null);
	}

	@Override
	public Producto save(Producto producto) {
		// TODO Auto-generated method stub
		return productoDao.save(producto);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		productoDao.deleteById(id);
	}

	@Override
	public Producto update(@RequestBody Producto producto) {
		// TODO Auto-generated method stub
		return productoDao.save(producto);
	}
	

}
