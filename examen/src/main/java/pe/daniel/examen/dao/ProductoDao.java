package pe.daniel.examen.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pe.daniel.examen.enity.Producto;

@Repository
public interface ProductoDao extends CrudRepository<Producto, Long> {

}
