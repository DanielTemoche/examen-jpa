package pe.daniel.examen.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pe.daniel.examen.enity.Categoria;

@Repository
public interface CategoriaDao extends CrudRepository<Categoria, Long> {

}
