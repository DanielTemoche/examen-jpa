package pe.daniel.examen.enity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="productos")
public class Producto implements Serializable { 
	private static final long serialVersionUID = 1L;
	
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long idproducto;
private int cantidad;
private String nombreprod;
private double precio;

@ManyToOne
private Categoria categoria;

public Long getIdproducto() {
	return idproducto;
}

public void setIdproducto(Long idproducto) {
	this.idproducto = idproducto;
}

public int getCantidad() {
	return cantidad;
}

public void setCantidad(int cantidad) {
	this.cantidad = cantidad;
}

public String getNombreprod() {
	return nombreprod;
}

public void setNombreprod(String nombreprod) {
	this.nombreprod = nombreprod;
}

public double getPrecio() {
	return precio;
}

public void setPrecio(double precio) {
	this.precio = precio;
}

public Categoria getCategoria() {
	return categoria;
}

public void setCategoria(Categoria categoria) {
	this.categoria = categoria;
}



}
