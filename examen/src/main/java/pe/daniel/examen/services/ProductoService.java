package pe.daniel.examen.services;

import java.util.List;

import pe.daniel.examen.enity.Producto;

public interface ProductoService {
	public List<Producto> findAll();
	public Producto findById(Long id);
	public Producto save(Producto producto);
	public void delete(Long id);
	public Producto update(Producto producto);
}
