package pe.daniel.examen.services;

import java.util.List;

import pe.daniel.examen.enity.Categoria;

public interface CategoriaService {
	public List<Categoria> findAll();
	public Categoria findById(Long id);
	public Categoria save(Categoria categoria);
	public void delete(Long id);
	public Categoria update(Categoria categoria);
}
