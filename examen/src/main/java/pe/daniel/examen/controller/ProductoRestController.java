package pe.daniel.examen.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pe.daniel.examen.enity.Producto;
import pe.daniel.examen.services.ProductoService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/inicio")
public class ProductoRestController {
	@Autowired
	private ProductoService productoService;
	
	@GetMapping("/producto")
	public List<Producto> readAll(){
		return productoService.findAll();
	}
	@GetMapping("/producto/{id}")
	public Producto read(@PathVariable Long id) {
		return productoService.findById(id);
	}
	@PostMapping("/producto/send")
	@ResponseStatus(HttpStatus.CREATED)
	public Producto create(@RequestBody Producto producto) {
		return productoService.save(producto);
	}
	@DeleteMapping("/producto/delete/{id}")
	public void delete(@PathVariable("id") Long id) {
		productoService.delete(id);
	}
	@PutMapping("/producto/update/{id}")
	public Producto update(@RequestBody Producto producto, @PathVariable Long id) {
		Producto actual_Producto = productoService.findById(id);
		actual_Producto.setNombreprod(producto.getNombreprod());
		actual_Producto.setPrecio(producto.getPrecio());
		actual_Producto.setCantidad(producto.getCantidad());
		actual_Producto.setCantidad(producto.getCantidad());
		return productoService.save(actual_Producto);
	}
}
